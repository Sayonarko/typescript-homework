export const ROUTE = {
    MOVIE: {
        POPULAR: '/movie/popular',
        TOP_RATED: '/movie/top_rated',
        UPCOMING: '/movie/upcoming',
        GET_MOVIE: '/movie/'
    },
    SEARCH: {
        MOVIE: '/search/movie'
    }
}