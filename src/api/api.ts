import { ROUTE } from './api.routes';
import fetchApi from '../helpers/fetchApi';

class MovieApi {

    async getPopular(urlParams: IurlParams): Promise<void> {
        return await fetchApi(ROUTE.MOVIE.POPULAR, 'GET', urlParams);
    }

    async getTopRated(urlParams: IurlParams): Promise<void> {
        return await fetchApi(ROUTE.MOVIE.TOP_RATED, 'GET', urlParams);
    }

    async getUpcoming(urlParams: IurlParams): Promise<void> {
        return await fetchApi(ROUTE.MOVIE.UPCOMING, 'GET', urlParams)
    }

    async getSearch(urlParams: IurlParams): Promise<void> {
        return await fetchApi(ROUTE.SEARCH.MOVIE, 'GET', urlParams)
    }

    async getMovieById(id: number): Promise<void> {
        return await fetchApi(ROUTE.MOVIE.GET_MOVIE + id, 'GET');
    }
}

const movieAPI = new MovieApi();
export default movieAPI;