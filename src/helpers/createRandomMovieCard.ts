export default function createRandomMovie(movies: Array<IMovie>, parent: HTMLElement): HTMLDivElement | undefined {
    if (movies.length) {
        const random = Math.floor(Math.random() * 20);
        const { backdrop_path, title, overview } = movies[random];
        const card = document.createElement('div');

        parent.style.cssText = `
        background: url('https://image.tmdb.org/t/p/original/${backdrop_path}') no-repeat top center;
        background-size: cover;
        `;

        card.className = 'row py-lg-5';
        card.innerHTML = `
        <div class="col-lg-6 col-md-8 mx-auto" style="background-color: #2525254f">
        <h1 id="random-movie-name" class="fw-light text-light">${title}</h1>
        <p id="random-movie-description" class="lead text-white">
                ${overview}
        </p>
    </div>
        `;
        return card;
    }

}