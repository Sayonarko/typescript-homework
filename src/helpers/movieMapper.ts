function pick<T, K extends keyof T>(obj: T, ...keys: K[]): Pick<T, K> {
    const result: any = {};
    keys.forEach(key => {
        result[key] = obj[key];
    })
    return result;
}


export default function movieMapper(data: any): Array<IMovie> {
    const results = data.results;
    const movies: Array<IMovie> = [];
    for (const el of results) {
        const result: IMovie = pick(el, 'poster_path', 'backdrop_path', 'title', 'overview', 'release_date', 'id');
        movies.push(result);
    }
    return movies
}
