import createMovieCard from './createMovieCard';
import movieAPI from '../api/api';

export default function renderFavoriteCards(): void {
    const favoriteContainer = document.getElementById('favorite-movies');
    let favorites: Array<number> = [];
    const localFavorites = localStorage.getItem('favorites');
    if (localFavorites) favorites = JSON.parse(localFavorites);

    if (favoriteContainer?.children.length) {
        while (favoriteContainer?.firstChild) favoriteContainer.firstChild.remove();
    }

    favorites.forEach(async (item) => {
        const movie: any = await movieAPI.getMovieById(item);
        const card = createMovieCard(movie);
        card.className = 'col-12 p-2';


        favoriteContainer?.appendChild(card);
    })
}