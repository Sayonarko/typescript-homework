import movieAPI from "./api/api";
import createMovieCard from "./helpers/createMovieCard";
import movieMapper from "./helpers/movieMapper";
import createRandomMovieCard from "./helpers/createRandomMovieCard";
import renderFavoriteCards from "./helpers/renderFavoriteCards";

export async function render(): Promise<void> {
    // TODO render your app here
    const searchInput = document.getElementById('search');
    const loadMoreBtn = document.getElementById('load-more');
    const movieContainer = document.getElementById('film-container');
    const randomMovieContainer = document.getElementById('random-movie');
    const radioBtns = document.querySelectorAll('input[name="btnradio"]');
    const favoriteBtn = document.getElementById('favorites-btn');

    let page = 1;
    let query = '';
    let category = 'popular';

    async function renderCards(page: number, category: string, query: string): Promise<void> {
        let data: any;

        switch (category) {
            case 'popular': data = await movieAPI.getPopular({ page });
                break;
            case 'upcoming': data = await movieAPI.getUpcoming({ page });
                break;
            case 'top_rated': data = await movieAPI.getTopRated({ page });
                break;
            case 'search': data = await movieAPI.getSearch({ page, query });
                break;
            default: data = await movieAPI.getPopular({ page });
        }

        const movies = movieMapper(data);
        const randomCard = createRandomMovieCard(movies, randomMovieContainer as HTMLElement);

        if (randomMovieContainer?.children.length && movies.length) {
            randomMovieContainer.removeChild(randomMovieContainer?.children[0]);
        }

        if (randomCard) randomMovieContainer?.appendChild(randomCard);

        movies.forEach(movie => {
            const card = createMovieCard(movie);
            movieContainer?.appendChild(card);
        })
    }

    function handleSearch(e: Event): void {
        const target = e.currentTarget as HTMLInputElement;
        page = 1;
        category = target.id;
        query = target.value;
        while (movieContainer?.firstChild) movieContainer.firstChild.remove();
        renderCards(page, category, query);
        target.value = '';
    }

    function handleSwitchCategory(e: Event): void {
        const { id } = e.currentTarget as HTMLInputElement;
        category = id;
        page = 1;
        while (movieContainer?.firstChild) movieContainer.firstChild.remove();
        renderCards(page, category, query);
    }

    function handleLoadMore(): void {
        page = page + 1
        renderCards(page, category, query);
    }

    searchInput?.addEventListener('change', (e) => handleSearch(e));
    radioBtns?.forEach(btn => btn.addEventListener('change', (e) => handleSwitchCategory(e)));
    loadMoreBtn?.addEventListener('click', () => handleLoadMore());
    favoriteBtn?.addEventListener('click', () => renderFavoriteCards());

    renderCards(page, category, query);
    renderFavoriteCards();
}
